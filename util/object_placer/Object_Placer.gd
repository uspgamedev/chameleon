tool
extends Sprite

export(bool) var running : bool = false setget set_run
export(Array, Texture) var objects : Array = []
export(int, 0, 4) var curr_object : int = 0 setget set_curr
export(PackedScene) var object_pos : PackedScene
export(bool) var place_object : bool = false setget place

func _ready():
	if not Engine.editor_hint:
		self.queue_free()

func set_run(value : bool) -> void:
	running = value
	self.curr_object = 0

func set_curr(value : int) -> void:
	if running:
		curr_object = value
		self.texture = objects[curr_object]

func place(_value : bool) -> void:
	if running:
		var new_pos = object_pos.instance()
		new_pos.texture = (self.curr_object+1)
		new_pos.transform = self.transform
		get_parent().add_child(new_pos)
		new_pos.set_owner(get_parent())
