tool
extends Position2D

export(int) var texture : int

remotesync func transform():
	var sprite : Sprite = Sprite.new()
	sprite.texture = load("res://assets/textures/camuflagem"+ str(texture) +".png")
	sprite.transform = self.transform
	get_parent().call_deferred("add_child", sprite)
