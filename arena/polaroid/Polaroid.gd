extends KinematicBody2D

class_name Polaroid

export var MOVE_SPEED := 200
export var FLASH_DURATION := 0.5
export var FLASH_COOLDOWN := 3.0

onready var velocity = Vector2()

func _ready():
	#Chamaleons ignore Blur
	if !is_network_master():
		$Blur.queue_free()
		$ProgressBar.hide()

func _input(event: InputEvent):
	if !is_network_master():
		return
	if $ProgressBar.value >= 100 and event.is_action_pressed("shoot"):
		$ProgressBar.value = 0
		rpc_unreliable("play_shutter_sfx")
		$Tween.interpolate_property(
			$Flash, "modulate", Color(1,1,1,1), Color(1,1,1,0), FLASH_DURATION, \
			Tween.TRANS_CUBIC, Tween.EASE_IN_OUT \
		)
		$Tween.start()
		for body in $SnapArea.get_overlapping_bodies():
			if (body as Node).is_in_group("fairy"):
				body.rpc("validate_snap", get_path())
			if (body as Node).is_in_group("chameleon"):
				body.rpc("validate_shock", get_path())

remotesync func play_shutter_sfx():
	$ShutterSFX.play()

func get_snap_area() -> Area2D:
	return $SnapArea as Area2D

func _physics_process(delta):
	if !is_network_master():
		return
	
	$ProgressBar.value += 100 * delta / FLASH_COOLDOWN
	
	var dir = Vector2()
	if Input.is_action_pressed("move_left"):
		dir.x = -1
	if Input.is_action_pressed("move_right"):
		dir.x = 1
	if Input.is_action_pressed("move_up"):
		dir.y = -1
	if Input.is_action_pressed("move_down"):
		dir.y = 1
	
	velocity = dir.normalized() * MOVE_SPEED
	
	#warning-ignore:return_value_discarded
	move_and_collide(velocity * delta)
	
	rpc_unreliable("update_state", self.position, $Flash.modulate)

puppet func update_state(pos: Vector2, flash: Color):
	self.position = pos
	$Flash.modulate = flash

remotesync func disable():
	set_physics_process(false)
	set_process_input(false)
