extends KinematicBody2D

export var SPEED := 150

func _physics_process(delta):
	if !is_network_master(): return
	
	var movement := Vector2.ZERO
	movement.x = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
	movement.y = Input.get_action_strength("move_down") - Input.get_action_strength("move_up")
	
	if abs(movement.x) > 0.0:
		$Sprite.scale.x = -sign(movement.x)
	
	#warning-ignore:return_value_discarded
	move_and_collide(movement * SPEED * delta)
	
	rpc("update_state", self.transform)

puppet func update_state(xform_state: Transform2D):
	self.transform = xform_state

func _process(_delta):
	for polaroid in get_tree().get_nodes_in_group("polaroid"):
		if polaroid.get_network_master() == get_tree().get_network_unique_id():
			hide()
