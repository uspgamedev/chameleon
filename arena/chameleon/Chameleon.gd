extends KinematicBody2D

class_name Chameleon

export var GRAVITY := 600.0
export var MOVE_SPEED := 120
export var JUMP_SPEED := 600
export var GHOST_SCENE: PackedScene

signal spawn_requested(node)

onready var velocity := Vector2()
onready var hiding := 0
onready var normal := Vector2.UP
onready var state := "walking"
onready var moving := 0

onready var jumped := false

func _ready():
	$Sprite/Head/AnimationPlayer.play("idle")
	var shader_material = $Sprite.get_material().duplicate(true)
	$Sprite.set_material(shader_material)
	$Sprite/Head.use_parent_material = true
	shader_material.set_shader_param("Shift_Hue", randf())

func _process(_delta):
	$Sprite.visible = self.state in ["shocked", "dying"] or (self.hiding == 0)
	for i in range($Camouflage.get_child_count()):
		$Camouflage.get_child(i).visible = (i+1 == self.hiding)
	
	if $AnimationPlayer.assigned_animation != self.state:
		$AnimationPlayer.play(self.state)
	if state == "walking":
		$AnimationPlayer.playback_speed = self.moving * $Sprite.scale.x
	else:
		$AnimationPlayer.playback_speed = 1
	
	if $Tongue.windup and $Sprite/Head/AnimationPlayer.current_animation == "idle":
		$Sprite/Head/AnimationPlayer.play("pero")
		$Sprite/Head/AnimationPlayer.queue("idle")

func _on_animation_finished(anim_name):
	if anim_name in ["landing", "shocked"]:
		self.state = "walking"
	elif anim_name == "dying" and is_network_master():
		rpc("remove")

func _input(event):
	if !is_network_master(): return
	if event is InputEventMouseButton and event.pressed and event.button_index == 1 \
									  and not self.state in ["shocked", "dying"]:
		$Tongue.throw()
	if event.is_action_pressed("jump") and !self.hiding and self.state == "walking":
		$JumpDelay.start()
		self.state = "jumping"
	
	for i in range(1,6):
		if Input.is_action_pressed("hide_%d" % i):
			if i != self.hiding and not self.state in ["shocked", "dying"]:
				self.hiding = i
			else:
				self.hiding = 0
			break

func _on_Jump_delay():
	if is_network_master():
		self.jumped = true

func has_collision() -> bool:
	for i in range(get_slide_count()):
		var coll = get_slide_collision(i)
		if !coll.collider.is_in_group("ceiling"):
			return true
	return false

func _physics_process(delta):
	if !is_network_master():
		return
	
	var disabled := false
	if self.state in ["shocked", "dying"]:
		self.normal = Vector2.UP
		disabled = true
	if !disabled and has_collision():
		if $CoyoteTimer.time_left > 0.0:
			$CoyoteTimer.stop()
		var coll := get_slide_collision(0)
		if self.jumped:
			var upwards = self.normal.dot(Vector2.UP)
			if upwards > 0:
				self.velocity = (coll.normal + Vector2.UP)/2 * JUMP_SPEED
			else:
				self.velocity = coll.normal * JUMP_SPEED/2
			self.normal = Vector2.UP
			self.jumped = false
		else:
			self.velocity = Vector2()
			self.normal = coll.normal
			if not self.state in ["walking", "shocked", "dying"] and $JumpDelay.time_left == 0.0:
				self.state = "landing"
	else:
		velocity -= self.normal * delta * GRAVITY
		if $CoyoteTimer.time_left == 0.0:
			$CoyoteTimer.start()
		if self.normal == Vector2.UP and velocity.y > 16 and not self.state in ["shocked", "dying"]:
			self.state = "falling"
		
		#velocity -= velocity.x * 0.5 * delta
	
	var target_angle = self.normal.angle() - (self.rotation - PI/2)
	if target_angle > PI: target_angle -= 2*PI
	if target_angle < -PI: target_angle += 2*PI
	self.rotation += target_angle * 5 * delta
	
	var move_dir = Vector2.ZERO
	if !self.hiding and $JumpDelay.time_left == 0.0:
		move_dir.x = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
		move_dir.y = Input.get_action_strength("move_down") - Input.get_action_strength("move_up")

	# Find closest direction along surface
	if move_dir.length_squared() > 0:
		var side1_dir = self.normal.rotated(PI/2.0)
		var side2_dir = self.normal.rotated(-PI/2.0)
		
		var diff = move_dir.dot(side1_dir) - move_dir.dot(side2_dir)
		if diff > 1:
			move_dir = side1_dir
			self.moving = -1
		elif diff < -1:
			move_dir = side2_dir
			self.moving = 1
		else:
			move_dir = Vector2.ZERO
			self.moving = 0
		
		velocity += move_dir * MOVE_SPEED * delta * 100
	else:
		self.moving = 0
	
	if abs(velocity.dot(move_dir)) > MOVE_SPEED :
		velocity -= move_dir * velocity.dot(move_dir)
		velocity += move_dir * MOVE_SPEED
	
	#Change Body Dir
	var mouse_dir = $Sprite/Head_pos.get_local_mouse_position()
	if mouse_dir.x > 64: $Sprite.scale.x *= -1

	#Move Head
	var head_angle = PI + $Sprite/Head_pos.get_local_mouse_position().angle()
	if head_angle > 0.52 and head_angle < 2.3: head_angle = 0.52
	elif head_angle >= 2.3 and head_angle < 5.61: head_angle = 5.61
	$Sprite/Head.rotation = head_angle

	$Debug.text = self.state
	#warning-ignore:return_value_discarded
	move_and_slide(velocity, Vector2.UP)
	
	rpc_unreliable("update_state", self.transform, self.hiding, self.moving,
								   self.state,\
								   $Sprite.transform, $Sprite/Head.rotation)
	
	if self.moving:
		play_ground_sfx()

func reset_gravity():
	if is_network_master() and !has_collision():
		self.normal = Vector2.UP

puppet func update_state(xform: Transform2D, hiding_state: int, \
						 moving_state: bool, current_state: String, \
						 sform: Transform2D, head_rotation: float):
	self.transform = xform
	self.hiding = hiding_state
	self.moving = moving_state
	self.state = current_state
	$Sprite.transform = sform
	$Sprite/Head.rotation = head_rotation

master func validate_shock(path: NodePath):
	if !has_node(path): return
	var node = get_node(path)
	if not node is Polaroid: return
	var polaroid := node as Polaroid
	var area := polaroid.get_snap_area()
	if self in area.get_overlapping_bodies():
		self.state = "shocked"
		self.hiding = 0

master func die():
	if get_tree().get_rpc_sender_id() != 1: return
	self.state = "dying"
	self.hiding = 0

remotesync func remove():
	var ghost = GHOST_SCENE.instance()
	ghost.position = self.position
	ghost.set_network_master(get_network_master())
	if has_node("Camera2D"):
		var cam = get_node("Camera2D")
		remove_child(cam)
		ghost.add_child(cam)
	emit_signal("spawn_requested", ghost)
	queue_free()

func play_jump_sfx():
	var i = randi() % $JumpSFX.get_child_count()
	$JumpSFX.get_child(i).play()
	
func play_ground_sfx():
	for i in range(get_slide_count()):
		var coll = get_slide_collision(i)
		if coll.collider.is_in_group("grass") and $GrassSFX.is_playing() == false:
			$GrassSFX.play()
		if coll.collider.is_in_group("water"):
			var j = randi() % $WaterSFX.get_child_count()
			if $WaterSFX.get_child(j).is_playing() == false:
				$WaterSFX.get_child(j).play()
