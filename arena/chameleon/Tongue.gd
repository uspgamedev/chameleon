extends Line2D

export var MAX_POINTS := 10
export var MIN_STEP_LENGTH := 20
export var MAX_STEP_LENGTH := 40
export var ANGLE_VARIATION := 30.0
export var LENGTH_VARIATION := 0.1
export var RECOIL_THRESHOLD := 5
export var TONGUE_MAX_LENGTH := 300

onready var recoil := 0.0

var has_fairy : bool = false
var releasing : bool = false
var windup: bool = false

func _ready():
	pass

func get_tip() -> Vector2:
	var count = get_point_count()
	if count > 0:
		return get_point_position(count - 1)
	else:
		return Vector2()

master func get_tip_path() -> NodePath:
	return $Tip.get_path()

func throw():
	if !is_network_master(): return
	if self.windup:
		return
	else:
		self.windup = true
		yield(get_tree().create_timer(0.125), "timeout")
		self.windup = false

	releasing = false
	if has_fairy: releasing = true

	var target = get_local_mouse_position()
	if target.length() > TONGUE_MAX_LENGTH:
		target = target.normalized()*TONGUE_MAX_LENGTH

	$Wall_Check.cast_to = target
	$Wall_Check.force_raycast_update()
	if $Wall_Check.get_collider() != null:
		target = to_local($Wall_Check.get_collision_point())

	var first_dir := Vector2(1, 0) * -sign(self.global_scale.x)
	
	clear_points()
	add_point(Vector2())
	add_point(first_dir * gen_step_len())
	
	for i in range(MAX_POINTS):
		var last_point = get_point_position(1 + i)
		var last_dir = last_point - get_point_position(0 + i)
		var expected_dir = target - last_point
		var expected_angle = expected_dir.angle() - last_dir.angle()
		if expected_angle > PI: expected_angle -= 2*PI
		if expected_angle < -PI: expected_angle += 2*PI
		var base_angle = expected_angle / (MAX_POINTS - i)
		var variation = deg2rad(ANGLE_VARIATION)
		var angle = base_angle + rand_range(-variation, variation)
		var dir = last_dir.rotated(angle)
		var length = expected_dir.length() / (MAX_POINTS - i)
		length *= rand_range(1.0 - LENGTH_VARIATION, 1.0 + LENGTH_VARIATION)
		var next_point = last_point + dir.normalized() * length
		add_point(next_point)
	add_point(target)
	if $Tween.is_active():
		$Tween.stop_all()
	$Tween.interpolate_property(self, "recoil", 1.0, 0.0, 0.25, Tween.TRANS_BACK, Tween.EASE_IN)
	$Tween.start()
	
	# Try to release fairy, they'll know whether that's the case
	$Tip.position = get_tip()
	for fairy in get_tree().get_nodes_in_group("fairy"):
		fairy.rpc("validate_release", self.get_path(), $Tip.get_path())

func _physics_process(_delta):
	if !is_network_master(): return
	$Tip.position = get_tip()
	rpc_unreliable("update_state", $Tip.position, self.windup)
	if get_point_count() > 1 and not releasing:
		for body in $Tip.get_overlapping_bodies():
			if body.is_in_group("fairy"):
				body.rpc("validate_capture", self.get_path(), $Tip.get_path())

master func fairy_captured(status : bool):
	if !status:
		yield(get_tree().create_timer(0.05), "timeout")
	has_fairy = status

puppet func update_state(pos: Vector2, windup_state: bool):
	$Tip.position = pos
	self.windup = windup_state

func _process(_delta):
	if !is_network_master(): return
	if get_point_count() > 0:
		var remove = []

		for i in range(1, get_point_count()):
			var current_point = get_point_position(i)
			var last_point = get_point_position(i-1)
			var diff = last_point - current_point
			if diff.length() < RECOIL_THRESHOLD:
				remove.append(i)
			else:
				var alpha = min(self.recoil, 1.02)
				var new = current_point * alpha + last_point * (1 - alpha)
				set_point_position(i, new)

		for i in range(remove.size()-1,-1,-1):
			remove_point(remove[i])

	rpc_unreliable("update_points", self.points)

func gen_step_len() -> float:
	return rand_range(MIN_STEP_LENGTH, MAX_STEP_LENGTH)

puppet func update_points(new_points: PoolVector2Array):
	self.points = new_points
