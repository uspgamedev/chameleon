extends KinematicBody2D

export var SPEED := 50
export var MIN_DELAY := 1.0
export var MAX_DELAY := 3.0
export var SMOOTH := 10

onready var acc_time := 0.0
onready var dir := Vector2()
onready var target_dir := Vector2()
onready var captured := false
onready var catcher_path: NodePath
onready var chamaleon: KinematicBody2D

func _ready():
	if is_network_master():
		self.target_dir = Vector2(1, 0).rotated(rand_range(0, 360))

func _physics_process(delta):
	if !is_network_master(): return
	
	if self.captured:
		if has_node(self.catcher_path):
			var catcher := get_node(self.catcher_path) as Node2D
			self.position = catcher.global_position
		else:
			self.captured = false
	
	if !self.captured:
		self.dir += (self.target_dir - self.dir) * SMOOTH * delta
		self.position += self.dir * SPEED * delta
	
	rpc("update_state", self.position, self.captured, self.visible)

func _on_timer_timeout():
	if is_network_master():
		var nests = $AvoidArea.get_overlapping_bodies()
		if nests.size() > 0:
			var away := Vector2.ZERO
			for nest in nests:
				away += (self.position - nest.position).normalized()
			away /= nests.size()
			self.target_dir = away
		else:
			self.target_dir = Vector2(1, 0).rotated(rand_range(0, 360))
		$Timer.wait_time = rand_range(MIN_DELAY, MAX_DELAY)
		$Timer.start()

master func validate_capture(tongue: NodePath, path: NodePath):
	if self.captured or !has_node(path) or $CatchCooldown.time_left > 0.0: return
	for fairy in get_tree().get_nodes_in_group("fairy"):
		if fairy != self and fairy.catcher_path == path:
			return

	var catcher := get_node(path) as Area2D
	if self in catcher.get_overlapping_bodies():
		self.catcher_path = path
		self.captured = true
		var tongue_node = get_node(tongue)
		tongue_node.rpc("fairy_captured", true)
		rpc_unreliable("play_eaten_sfx")
		self.chamaleon = tongue_node.get_parent()

remotesync func play_eaten_sfx():
	$EatenSFX.play()

master func validate_release(tongue: NodePath, path: NodePath):
	if !self.captured or !has_node(path) or self.catcher_path != path: return
	self.show()
	self.captured = false
	self.catcher_path = ""
	self.chamaleon = null
	$CatchCooldown.start()
	self.position = get_node(path).global_position
	get_node(tongue).rpc("fairy_captured", false)

master func validate_snap(path: NodePath):
	if !has_node(path): return
	var node = get_node(path)
	if not node is Polaroid: return
	var polaroid := node as Polaroid
	var area := polaroid.get_snap_area()
	if self in area.get_overlapping_bodies():
		self.captured = false
		$CatchCooldown.start()
		rpc("explode")

remotesync func explode():
	$Ring.rotation = randf() * 2 * PI
	$AnimationPlayer.play("explode")
	$AnimationPlayer.queue("default")

func kill_chameleons():
	if !is_network_master(): return
	for body in $ExplosionArea.get_overlapping_bodies():
		if body is Chameleon:
			body.rpc("die")

func _process(delta):
	if captured:
		$Sphere.hide()
		$Particles2D.emitting = false
		$AnimatedSprite.hide()

		if is_network_master():
			if chamaleon.hiding: self.hide()
			else: self.show()

	else:
		$Sphere.show()
		$Particles2D.emitting = true
		$AnimatedSprite.show()
		self.acc_time += delta
		var intensity := sin(0.5 * 2.0 * PI * self.acc_time)
		$Light.energy = 1.0 + 0.1 * intensity
		$Sphere.material.set_shader_param("intensity", intensity)

puppet func update_state(pos: Vector2, cap: bool, vision: bool):
	self.position = pos
	self.captured = cap
	self.visible = vision

remotesync func remove():
	queue_free()
