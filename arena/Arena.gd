extends Node2D

export var MAX_FAIRIES := 10
export var FAIRY_SCENE: PackedScene
export var SPAWN_AREA: Rect2

signal polaroid_won

#warning-ignore:unused_signal
signal fairy_delivered

func _ready():
	if !is_network_master(): return
	for nest in get_tree().get_nodes_in_group("nest"):
		nest.connect("fada_na_toca", self, "emit_signal", ["fairy_delivered"])
	shuffle_nests()

	for object_group in $Objects.get_children():
		var object_shuffled : Array = object_group.get_children()
		object_shuffled.shuffle()
		for i in range(int(object_shuffled.size()*0.4)):
			object_shuffled[i].rpc("transform")

func _physics_process(_delta):
	if !is_network_master(): return
	
	var fairies = get_tree().get_nodes_in_group("fairy")
	if fairies.size() < MAX_FAIRIES:
		var pos = Vector2()
		pos.x = SPAWN_AREA.position.x + rand_range(0, SPAWN_AREA.size.x)
		pos.y = SPAWN_AREA.position.y + rand_range(0, SPAWN_AREA.size.y)
		rpc("place_fairy", pos)
	
	if get_tree().get_nodes_in_group("chameleon").size() == 0:
		emit_signal("polaroid_won")

func shuffle_nests():
	if !is_network_master(): return
	
	var nests = get_tree().get_nodes_in_group("nest")
	var active_count = int(ceil(nests.size() / 2.0))
	var active = []
	for _i in range(active_count):
		var j = randi() % nests.size()
		while j in active:
			j = randi() % nests.size()
		active.push_back(j)
	for i in range(nests.size()):
		nests[i].toca_status =  i in active

func place_entity(entity: Node):
	$Entities.add_child(entity)
	if entity.has_signal("spawn_requested"):
		#warning-ignore:return_value_discarded
		entity.connect("spawn_requested", self, "place_entity")

remotesync func place_fairy(pos: Vector2):
	if get_tree().get_rpc_sender_id() == 1:
		var fairy = FAIRY_SCENE.instance()
		fairy.position = pos
		fairy.set_network_master(1)
		$Entities.add_child(fairy)
