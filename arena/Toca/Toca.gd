extends StaticBody2D

var toca_status = true
export var toca_tex1: Texture
export var toca_tex2: Texture

onready var dir := Vector2()

signal fada_na_toca()

func _process(_delta):
	var polaroids = get_tree().get_nodes_in_group("polaroid")
	var can_see = polaroids.size() == 0 \
			   or (polaroids[0] as Node).get_network_master() != get_tree().get_network_unique_id()
	if can_see and toca_status == true:
		$tocaSprite.set_texture(toca_tex1)
	else:
		$tocaSprite.set_texture(toca_tex2)

func _on_timer_timeout():
	if is_network_master():
		$Timer.start()

puppet func update_state(status: bool):
	self.toca_status = status

func _physics_process(_delta):
	if !is_network_master(): return
	var delivered := false
	if self.toca_status:
		for body in $DeliveryArea.get_overlapping_bodies():
			if body.is_in_group("fairy") and !body.captured:
				emit_signal("fada_na_toca")
				delivered = true
				body.rpc("remove")
		if delivered:
			rpc_unreliable("play_delivered_sfx")
			$TocaLight.energy = 1
			$TocaParticles.emitting = true
			yield(get_tree().create_timer(2), "timeout")
			$TocaLight.energy = 0
			$TocaParticles.emitting = false
	rpc("update_state", self.toca_status)

remotesync func play_delivered_sfx():
	$DeliveredSFX.play()

# Abaixo segue o código Arena.gd para tentar instanciar uma toca (a parte modificada está com asterisco na frente):


#extends Node2D
#
#export var MAX_FAIRIES := 10
#export var FAIRY_SCENE: PackedScene
#*export var TOCA_SCENE: PackedScene
#export var SPAWN_AREA: Rect2
#
#func _physics_process(_delta):
#	if !is_network_master(): return
#
#	var fairies = get_tree().get_nodes_in_group("fairy")
#	if fairies.size() < MAX_FAIRIES:
#		var pos = Vector2()
#		pos.x = SPAWN_AREA.position.x + rand_range(0, SPAWN_AREA.size.x)
#		pos.y = SPAWN_AREA.position.y + rand_range(0, SPAWN_AREA.size.y)
#		rpc("place_fairy", pos)
#
#*	var toca = get_tree().get_nodes_in_group("Toca")
#
#*	var pos_toca = Vector2()
#*	pos_toca.x = SPAWN_AREA.position.x + 200
#*	pos_toca.y = SPAWN_AREA.position.y + 200
#*	rpc("place_toca", pos_toca)
#
#
#func place_entity(entity: Node):
#	$Entities.add_child(entity)
#
#remotesync func place_fairy(pos: Vector2):
#	if get_tree().get_rpc_sender_id() == 1:
#		var fairy = FAIRY_SCENE.instance()
#		fairy.position = pos
#		fairy.set_network_master(1)
#		$Entities.add_child(fairy)
#
#*remotesync func place_toca(toca_pos: Vector2):
#*	var toca = TOCA_SCENE.instance()
#*	toca_pos.x = SPAWN_AREA.position.x + 200
#*	toca_pos.y = SPAWN_AREA.position.y + 200
#*	toca.position = toca_pos
#*	$Entities.add_child(toca)
