extends CanvasLayer

signal host_requested(port)
signal connect_requested(server_ip, server_port)
#warning-ignore:unused_signal
signal start_game_requested
#warning-ignore:unused_signal
signal end_connection_requested
#warning-ignore:unused_signal
signal chameleons_won

func get_player_name() -> String:
	return $Menu/Actions/Buttons/NameInput.text as String

func _on_connect_button_pressed():
	var server_ip := $Menu/Actions/Buttons/ConnectIP.text as String
	var server_port := $Menu/Actions/Buttons/ConnectPort.value as int
	emit_signal("connect_requested", server_ip, server_port)

func _on_host_button_pressed():
	var port := $Menu/Actions/Buttons/HostPort.value as int
	emit_signal("host_requested", port)

func hide_menu():
	$Menu.hide()
	$Score.show()

func show_menu():
	$Menu.show()
	$Score.hide()

remotesync func show_polaroid_win():
	if get_tree().get_rpc_sender_id() == 1:
		$PolaroidWin.show()

remotesync func show_chameleons_win():
	if get_tree().get_rpc_sender_id() == 1:
		$ChameleonsWin.show()

func _on_chameleons_won():
	if is_network_master():
		emit_signal("chameleons_won")
