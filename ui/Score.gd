extends Label

export var GOAL := 10

onready var current := 0

signal chameleons_won

func _on_fairy_delivered():
	if !is_network_master(): return
	self.current += 1
	if self.current >= GOAL:
		emit_signal("chameleons_won")

func _process(_delta):
	self.text = "%d/%d" % [self.current, GOAL]
