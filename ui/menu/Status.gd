extends Label

func _refresh(lobby: Lobby):
	if !lobby.has_connection():
		self.text = "OFFLINE"
	elif lobby.is_server():
		self.text = "OPEN AS SERVER"
	elif lobby.is_client():
		self.text = "CONNECTED AS CLIENT"
