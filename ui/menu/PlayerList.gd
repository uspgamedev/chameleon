extends ItemList

func _refresh(lobby: Lobby):
	self.clear()
	for id in lobby.player_info:
		var label = lobby.player_info[id]["name"]
		if id == get_tree().get_network_unique_id():
			label += " (you)"
		self.add_item(label, null, false)
