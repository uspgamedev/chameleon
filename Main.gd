extends Control

export var arena_scene: PackedScene
export var chameleon_scene: PackedScene
export var polaroid_scene: PackedScene

onready var players_done = []

func _process(_delta):
	var player_name = $UI.get_player_name()
	if len(player_name) == 0:
		player_name = "Player"
	$Lobby.my_info["name"] = player_name
	for watcher in get_tree().get_nodes_in_group("watch_lobby"):
		watcher._refresh($Lobby)

func start_game():
	if $Lobby.is_server():
		print("start")
		randomize()
		var peers = $Lobby.player_info.keys()
		var polaroid
		if peers.size() > 1:
			polaroid = peers[randi() % peers.size()]
		else:
			polaroid = -1
		print("Polaroid is %s" % polaroid)
		rpc("pre_configure_game", polaroid)

remotesync func pre_configure_game(polaroid_peer):
	print("pre configuring")
	get_tree().paused = true
	var self_peer_id = get_tree().get_network_unique_id()
	
	# Load arena
	var arena = arena_scene.instance()
	arena.connect("polaroid_won", self, "_on_polaroid_win")
	arena.connect("fairy_delivered", $UI/Score, "_on_fairy_delivered")
	add_child(arena)

	# Load players
	for p in $Lobby.player_info:
		var player
		if p == polaroid_peer:
			player = polaroid_scene.instance()
		else:
			player = chameleon_scene.instance()
		if p == self_peer_id:
			var camera = preload("res://arena/Camera2D.tscn").instance()
			camera.name = "Camera2D"
			camera.current = true
			player.add_child(camera)
		player.position = arena.get_node("PlayerSpawn").position
		player.set_name(str(p))
		player.set_network_master(p) # Will be explained later
		arena.place_entity(player)

	# Tell server (remember, server is always ID=1) that this peer is done pre-configuring.
	# The server can call get_tree().get_rpc_sender_id() to find out who said they were done.
	if self_peer_id == 1:
		mark_done(1)
	else:
		rpc_id(1, "done_preconfiguring")

remote func done_preconfiguring():
	var who = get_tree().get_rpc_sender_id()
	# Here are some checks you can do, for example
	mark_done(who)

func mark_done(who):
	print("done configuring")
	assert(get_tree().is_network_server())
	assert(who in $Lobby.player_info) # Exists
	assert(not who in players_done) # Was not added yet

	players_done.append(who)

	if players_done.size() == $Lobby.player_info.size():
		rpc("post_configure_game")

remotesync func post_configure_game():
	print("post configuring")
	# Only the server is allowed to tell a client to unpause
	if 1 == get_tree().get_rpc_sender_id():
		get_tree().paused = false
		$UI.hide_menu()
		# Game starts now!

func _on_polaroid_win():
	if is_network_master():
		$UI.rpc("show_polaroid_win")

func _on_chameleons_win():
	if is_network_master():
		$UI.rpc("show_chameleons_win")
		for polaroid in get_tree().get_nodes_in_group("polaroid"):
			polaroid.disable()
