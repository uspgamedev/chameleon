extends Node

class_name Lobby

export var max_players := 5

onready var player_info := {}
onready var my_info := { name = "Player" }

func _ready():
	#warning-ignore:return_value_discarded
	get_tree().connect("network_peer_connected", self, "_player_connected")
	#warning-ignore:return_value_discarded
	get_tree().connect("network_peer_disconnected", self, "_player_disconnected")
	#warning-ignore:return_value_discarded
	get_tree().connect("connected_to_server", self, "_connected_ok")
	#warning-ignore:return_value_discarded
	get_tree().connect("connection_failed", self, "_connected_fail")
	#warning-ignore:return_value_discarded
	get_tree().connect("server_disconnected", self, "_server_disconnected")

func init_server(port: int):
	#var peer = WebSocketServer.new()
	var peer = NetworkedMultiplayerENet.new()
	peer.create_server(port, self.max_players)
	get_tree().network_peer = peer
	player_info[1] = my_info
	print("Server started")

func init_client(server_ip: String, server_port: int):
	#var peer = WebSocketClient.new()
	var peer = NetworkedMultiplayerENet.new()
	peer.create_client(server_ip, server_port)
	get_tree().network_peer = peer
	player_info[get_tree().get_network_unique_id()] = my_info

func has_connection() -> bool:
	return get_tree().network_peer != null

func is_server() -> bool:
	return has_connection() and get_tree().is_network_server()

func is_client() -> bool:
	return has_connection() and !get_tree().is_network_server()

func end_connection():
	get_tree().network_peer = null
	player_info.clear()
	print("Connection ended")

func _player_connected(id):
	# Called on both clients and server when a peer connects. Send my info to it.
	rpc_id(id, "register_player", my_info)

func _player_disconnected(id):
	#warning-ignore:return_value_discarded
	player_info.erase(id) # Erase player from info.

func _connected_ok():
	pass # Only called on clients, not server. Will go unused; not useful here.

func _server_disconnected():
	pass # Server kicked us; show error and abort.

func _connected_fail():
	pass # Could not even connect to server; abort.

remote func register_player(info):
	# Get the id of the RPC sender.
	var id = get_tree().get_rpc_sender_id()
	# Store the info
	player_info[id] = info
